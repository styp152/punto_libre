<?php
     require('cabecera.php');
     require('conexion.php');
     conexion();
?>
        <div class="container-fluid">
            <div class="row text-center">
                <h2>Créditos</h2>
                <p>Este sistema fue realizado en la Universidad Politécnica Territorial del Estado Mérida (UPTMKR)</p>
                <strong>Bajo la Tutoria de:</strong>
            </div>
	    <div class="row-fluid">
		<div class="span12">
                    <div class="span6 well">
			<h4>Tutores Académicos</h4>
		    </div>
                    <div class="span6 well">
			<h4>Tutores en la comunidad</h4>
		    </div>
		</div>
	    </div>
	    <div class="row text-center">
		<strong>Fue desarrollado por:</strong>
	    </div>
	    <div class="span1"></div>
            <div class="row-fluid text-center">
		<div class="span10 well">
			<h4>Módulo: Administración de Punto Libre</h4>
			<p>EL módulo de Administración fue desarrollado por los estudiantes:</p>
                        <div class="row-fluid">
                            <div class="span3">
                                <div class="thumbnail">
                                    <img class="fotos" src="../img/personas/joenny.jpg" alt="Joeinny">
                                </div>
                                <div class="caption">
                                    <h4>Joeinny Osorio</h4>
                                    <a href="https://plus.google.com/u/0/115929106295429767060/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Joeinny Osorio</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
                                        <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
			    <div class="span3">
                                <div class="thumbnail">
                                    <img class="fotos" src="../img/personas/ericka.jpg" alt="Ericka">
                                </div>
                                <div class="caption">
                                    <h4>Ericka Simancas</h4>
                                    <a href="https://plus.google.com/u/0/114208653607050877714/posts" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Ericka Simancas</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
			    <div class="span3">
				<div class="thumbnail">
					<img class="fotos" src="../img/personas/nelly.jpg" alt="Nelly">
				</div>
				<div class="caption">
					<h4>Nelly Ortiz</h4>
					<a href="https://plus.google.com/u/0/102106319001069528872/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
					    <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Nelly Ortiz</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					    <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
					</a>       
				</div>
			    </div>
			    <div class="span3">
				<div class="thumbnail">
					<img class="fotos" src="../img/personas/richard.jpg" alt="Richard">
				</div>
				<div class="caption">
					<h4>Richard Torres</h4>
					<a href="https://plus.google.com/u/0/109784449217029605372/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
					    <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Richard Torres</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					    <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
					</a>       
				</div>
			    </div>
			</div>
                </div>
		<div class="span2"></div>
		<div class="row-fluid">
		    <div class="span8 well">
                        <h4>Módulo: Gestión de Cursos</h4>
                        <p>EL módulo de gestión de cursos fue desarrollado por los estudiantes:</p>
                        <div class="row-fluid thumbnail">
                            <div class="span4">
				<div class="thumbnail">
					<img class="fotosv" src="../img/personas/yuberli.jpg" alt="Yuberli">
				</div>
				<div class="caption">
                                    <h4>Yuberli Vielma</h4>
                                    <a href="https://twitter.com/Yuberli1" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @yubervielma</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script><br />
                                    <a href="https://plus.google.com/u/0/100039679485355924117/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Yuberli Vielma</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
                                        <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="thumbnail">
                                    <img class="fotosv" src="../img/personas/germain.jpg" alt="Germain">
                                </div>
                                <div class="caption">
                                    <h4>Germain Rojas</h4>
                                    <a href="https://twitter.com/germaintherock" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @germaintherock</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script><br />
                                    <a href="https://plus.google.com/u/0/103066729516969691577/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Germain Rojas</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="thumbnail">
                                    <img class="fotosv" src="../img/personas/miguel.jpg" alt="MASC">
                                </div>
                                <div class="caption">
				    <h4>Miguel Sanabria</h4>
				    <a href="https://twitter.com/masc1293" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @masc1293</a>
				    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script><br />
				    <a href="https://plus.google.com/u/0/118150581712122235423/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
					<span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Miguel Sanabria</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
				    </a>       
				</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    <div class="row-fluid">
		<div class="span12">
		    <div class="span6 well">
			<h4>Módulo: Control de Inventario</h4>
			<p>EL módulo de control de inventario fue desarrollado por los estudiantes:</p>
                        <div class="row-fluid thumbnail">
                            <div class="span4">
                                <div class="thumbnail">
                                    <img class="fotosv" src="../img/personas/iris.png" alt="Iris">
                                </div>
                                <div class="caption">
                                    <h4>Iris Diaz</h4>
                                    <a href="https://plus.google.com/u/0/107801046554205949051/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Iris Diaz</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
                                        <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="thumbnail">
                                    <img class="fotosv" src="../img/personas/holgui.png" alt="Holgui">
                                </div>
                                <div class="caption">
                                    <h4>Holgui Vega</h4>
                                    <a href="https://plus.google.com/u/0/109607562815931019257/posts" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Holgui Vega</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="thumbnail">
                                    <img class="fotosv" src="../img/personas/mariangela.png" alt="mariangela">
                                </div>
                                <div class="caption">
				    <h4>María Teran</h4>
				    <a href="https://plus.google.com/u/0/108778671931350206005/posts"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
					<span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">María Teran</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
					<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
				    </a>       
				</div>
                            </div>
			</div>
		    </div>
		    <div class="span6 well">
			<h4>Módulo: Gestión de Soporte a Equipos</h4>
			<p>EL módulo de gestión de soporte a equipos fue desarrollado los estudiantes:</p>
			<div class="row-fluid thumbnail">
			    <div class="span4">
				<div class="thumbnail">
				    <img class="fotosv" src="../img/personas/jesus.png" alt="hola">
				</div>
				<div class="caption">
                                    <h4>Jesús Vielma</h4>
                                    <a href="https://twitter.com/chuyquien" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @chuyquien</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script><br />
                                    <a href="//plus.google.com/108178194666820594837?prsrc=3"rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                        <span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Jesús Vielma</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
                                        <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
                                    </a>
                                </div>
                            </div>
			    <div class="span4">
				<div class="thumbnail">
				    <img class="fotosv" src="../img/personas/engelbert.png" alt="hola">
				</div>
				<div class="caption">
                                    <h4>Engelbert Portillo</h4>
                                    <a href="https://twitter.com/alexangelbert" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @alexangelbert</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                </div>
                            </div>
			    <div class="span4">
				<div class="thumbnail">
				    <img class="fotosv" src="../img/personas/pedro.png" alt="hola">
				</div>
				<div class="caption">
                                    <h4>Pedro Peralta</h4>
                                    <a href="https://twitter.com/PetterV27" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @PetterV27</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>
<?php
require('piepagina.php');
?>
