<?php
     require('cabecera.php');
     require('menu.php');
     require('conexion.php');
     
     $codigo = $_GET['ci_instru'];
     $sql = "SELECT * FROM instructor WHERE instructor.ci_instru=$codigo LIMIT 0,1";

     $result = mysql_query($sql);
     $row = mysql_fetch_assoc($result);
?>
     <div class="span9"><!--Contenido-->
	  <div class="hero-unit"><!--Bloque de Contenido Gris-->
	       <h3 class="text-center">Modificar registro del Instructor</h3>
	       <form method="post" action="instructor_actualizar.php">
		   <div class="row-fluid">
		       <div class="span12 text-center btn-primary">
			   <span>Datos del Instructor</span>
		       </div>
		   </div>
		   <div class="row-fluid">
		       <div class="span12"><br />
			   <div class="row-fluid">
			       <div class="span2"><span>Cédula:</span></div>
			       <div class="span3">
				   <input name="ci_instru" type="text" class="input-block-level text-center" readonly value="<?=$row['ci_instru']?>">
			       </div>
			       <div class="span2"></div>
			       <div class="span2"><span>Teléfono:</span></div>
			       <div class="span3">					    
				   <input name="tlfn_instru" type="text"  class="input-block-level text-center" maxlength="12" value="<?=$row['tlfn_instru']?>" title="Solo debe introducir números" pattern="[0-9]{4}[-][0-9]{7}" required>
			       </div>
			   </div>
			   <div class="row-fluid">
			       <div class="span2"><span>Nombres:</span></div>
			       <div class="span3">
				   <input name="nombre_instru" type="text" class="input-block-level text-center" maxlength="50" value="<?=$row['nombre_instru']?>" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{3,}" required>
			       </div>
			       <div class="span2"></div>
			       <div class="span2"><span>Correo:</span></div>
			       <div class="span3">
				   <input name="correo_instru" type="email"  class="input-block-level text-center" maxlength="30" value="<?=$row['correo_instru']?>">
			       </div>
			   </div>
			   <div class="row-fluid">
			       <div class="span2"><span>Apellidos:</span></div>
			       <div class="span3">
				   <input name="apellido_instru" type="text"  class="input-block-level text-center" maxlength="50" value="<?=$row['apellido_instru']?>" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{3,}" required>
			       </div>
			       <div class="span2"></div>
			       <div class="span2"><span>Ocupación:</span></div>
			       <div class="span3">
				   <select name="ocupacion_instru" class="input-block-level" title="Seleccione la ocupación del Instructor">
					<?php
					     $busq_m=mysql_query("select * from instructor ORDER BY ocupacion_instru");
					     while($reg_m=mysql_fetch_array($busq_m))
					     {
					     if($row['ocupacion_instru']==$reg_m['ocupacion_instru'])
					     echo "<option value='".$reg_m['ocupacion_instru']."' >".$reg_m['ocupacion_instru']."</option>";
					     }
					?>
					<option>Estudiante</option>
					<option>Profesional</option>
				   </select>
			       </div>
			   </div><br>
			   <div class="row-fluid">
			      <div class="span2"><span>Género:</span></div>
			      <div class="span7" title="Elija el Genero del Instructor">
				       Masculino <input type="radio" name="genero_instru" value="Masculino"
				       <?php if ($row['genero_instru'] == "Masculino") { echo "checked=\"checked\"";} ?> onClick="selec()"/>
				       Femenino <input type="radio" name="genero_part" value="Femenino"
				       <?php if ($row['genero_instru'] == "Femenino") { echo "checked=\"checked\"";} ?> onClick="selec()"/>
			      </div>
			   </div><br>
			   <div class="row-fluid">
			       <div class="span2"><span>Dirección:</span></div>
			       <div class="span7">					    
				   <textarea name="direccion_instru"cols="64" rows="5" title="Puede introducir letras, Simbolos y Números" required><?=$row['direccion_instru']?></textarea>
			       </div>
			   </div>
			 </div>
		    </div>
		    <hr><br />
		    <div class="row-fluid">
			 <div class="span12 text-center">
			   <button type="submit" class="btn btn-primary"><i class="icon-retweet icon-white"></i> Actualizar</button>
			    <a class="btn" href="instructor_consultar.php" role="button"><i class="icon-hand-left"></i> Volver</a>
			 </div>
		    </div>
	       </form><!--cierre del formulario-->
	       </div><!--cierre del Hero-Unit-->
	  </div><!--cierre del contenido-->
     </div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<?php
require('piepagina.php');
?>

