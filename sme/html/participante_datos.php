<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');

     
if (isset($_POST['ci_part']))
$codigo= $_POST['ci_part'];
else
$codigo = null;
$sql = "SELECT * FROM participante WHERE participante.ci_part= '$codigo'";
$result = mysql_query($sql);
if(mysql_num_rows($result)!=0)
{
     $row = mysql_fetch_assoc($result);
}
else
{
     echo "<script type='text/javascript'> alert('Cedula no Registrada en el Sistema'); window.location='participante_registro.php';</script>";

}
?>
<div class="span9"><!--Contenido-->
     <div class="hero-unit"><!--Bloque de Contenido Gris-->
	  <h3 class="text-center">Datos del Participante</h3><hr>
	  <div class="span3"></div>
	       <div class="span6">
		    <table class="table table-striped" border=3 >
			 <tr>
			     <th>Cédula</th>
			     <td><?=$row['ci_part']?></td>
			 </tr>
			 <tr>
			     <th>Nombre</th>
			     <td><?=$row['nombre_part']?></td>
			 </tr>
			 <tr>
			     <th>Apellido</th>
			     <td><?=$row['apellido_part']?></td>
			 </tr>
			 <tr>
			     <th>Género</th>
			     <td><?=$row['genero_part']?></td>
			 </tr>
			 <tr>
			     <th>Dirección</th>
			     <td><?=$row['direccion_part']?></td>
			 </tr>
			 <tr>
			     <th>Telefóno</th>
			     <td><?=$row['tlfn_part']?></td>
			 </tr>
			 <tr>
			     <th>Correo</th>
			     <td><?=$row['correo_part']?></td>
			 </tr>
			 <tr>
			     <th>Ocupación</th>
			     <td><?=$row['ocup_part']?></td>
			 </tr>
		    </table>
	       </div>
	  <div class="row-fluid">
	     <div class="span12 text-center"><hr>
		 <a class="btn btn-info" href="participante_modificar.php?ci_part=<?php echo $codigo;?>"><i class="icon-pencil icon-white"></i> Modificar</a>				
		 <a class="btn btn-primary" href="inscripcion.php" role="button"><i class="icon-file icon-white"></i> Inscribir</a>
		<a class="btn" href="participante_consultar.php"><i class="icon-hand-left"></i> Volver</a>	
	     </div>
	 </div>
     </div><!--cierre del Hero-Unit-->
</div><!--cierre del contenido-->
</div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<?php
require('piepagina.php');
?>
