<?php
     require('cabecera.php');
     require('menu.php');
     require('conexion.php');
     
     if (isset($_GET['codigo'])) $codigo= $_GET['codigo'];
     else $codigo = null;
     $sql = "SELECT * FROM curso, instructor, horario WHERE curso.ci_instru=instructor.ci_instru and curso.cod_curso=horario.cod_curso and curso.cod_curso= '$codigo'";
     $result = mysql_query($sql);
     $row = mysql_fetch_assoc($result);
     
?>
	  <div class="span9"><!--Contenido-->
	       <div class="hero-unit"><!--Bloque de Contenido Gris-->
		    <h3 class="text-center"><?=$row['nombre_curso']?></h3>
		    <div class="row-fluid">
			<div class="span12 text-center btn-primary">
			    <span>Descripcion del Curso</span>
			</div>
		     </div><br>
		     <div class="row-fluid">
			<div class="span2 borde1">
			    <img src="<?=$row['img_curso']?>"/>
			</div>
			<div class="span10 borde1">
			    <p><?=$row['descripcion_curso']?></p>
			</div>
		     </div><br>
		     <div class="row-fluid">
		    <div class="span6 borde1">
			<b>Temas:</b>
			<?=$row['tema_curso']?><br>
		    </div>
		    <div class="span6 borde1">
			<p>
			    <b>Fecha inicio: </b><?=$row['fechaini_curso']?><br />
			    <b>Fecha Fin: </b><?=$row['fechafin_curso']?><br />
			    <b>Días: </b><?=$row['dias_curso']?><br />
			    <b>Horario: </b><?=$row['horaini_curso']?> a <?=$row['horafin_curso']?><br />
			    <b>Costo: </b><?=$row['costo_curso']?><br />
			    <b>Instructor: </b><?=$row['nombre_instru']?> <?=$row['apellido_instru']?><br />
			    <b>Certificado: </b><?=$row['certi_curso']?><br />
			</p>
		    </div>
		    </div>
		    <hr><br />
		    <div class="row-fluid">
			 <div class="span9 text-center">
			      <a class="btn btn-primary" href="inscripcion.php" role="button"><i class="icon-file icon-white"></i> Inscribir</a>
			      <a class="btn btn-success" href="curso_participante.php?codigo=<?php echo $codigo;?>" role="button"><i class="icon-user icon-white"></i> Participantes Inscritos</a>
			      <a class="btn btn-info" href="curso_modificar.php?cod_curso=<?php echo $codigo;?>"><i class="icon-pencil icon-white"></i> Modificar</a>				
			      <a class="btn" href="curso_consultar.php" role="button"><i class="icon-hand-left"></i> Volver</a>
			 </div>
			 <div class="span3 text-right">
			      <a class="btn btn-danger" href="#myModal" data-toggle="modal"><i class="icon-remove icon-white"></i> Eliminar Curso</a>				
			 </div>
		    </div>
	       </div><!--cierre del Hero-Unit-->
	  </div><!--cierre del contenido-->
     </div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<!-- ----------------------------------------------------------------------------------------------------- -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header ">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Advertencia de Seguridad</h3>
</div>
<div class="modal-body alert alert-block">
<strong><p>¿Esta totalmente seguro que desea eliminar el curso.?</p></strong>
</div>
<div class="modal-footer">
<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Cancelar</button>
<button type="submit" class="btn btn-danger" onclick="location.href='curso_eliminar.php?cod_curso=<?php echo $codigo;?>'"><i class="icon-file icon-white"></i>Eliminar</button>
</div>
</div>
<!-- ----------------------------------------------------------------------------------------------------- -->
<?php
require('piepagina.php');
?>