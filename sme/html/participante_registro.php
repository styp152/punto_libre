<?php
     require('cabecera.php');
     require('menu.php');
    
     $con= @mysql_connect("localhost","root","21183652");
     
     if(!mysql_select_db("administracion", $con)){
         echo "error al conectarse";}
         include("../../Sistema_administrativo/php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Acceso_Cursos']==1 and $_SESSION['Insertar_Informacion']==1){
     
     require('conexion.php');
?>
<div class="span9"><!--Contenido-->
     <div class="hero-unit"><!--Bloque de Contenido Gris-->
	<h3 class="text-center">Registro del Participante</h3>
	<form method="post" action="participante_insertar.php">
	    <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Datos del Participante</span>
		</div>
	    </div>
	    <div class="row-fluid">
		<div class="span12"><br />
		    <div class="row-fluid">
			 <div class="span2"><span>Cédula:</span></div>
			 <div class="span3">
			      <input name="ci_part" class="input-block-level text-center" type="text" placeholder="00000000" title="Solo debe introducir Números" pattern="[0-9]{0,9}" required>
			 </div>
			 <div class="span2"></div>
			 <div class="span2"><span>Teléfono:</span></div>
			 <div class="span3">					    
			      <input name="tlfn_part" class="input-block-level text-center" type="text" maxlength="12" placeholder="0000-0000000" title="Solo debe introducir números" pattern="[0-9]{4}[-][0-9]{7}" required>
			 </div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Nombres:</span></div>
			<div class="span3">
			    <input name="nombre_part" class="input-block-level text-center" type="text" maxlength="39" placeholder="Nombres del Participante" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{3,}" required>
			</div>
			<div class="span2"></div>
			<div class="span2"><span>Correo:</span></div>
			<div class="span3">
			    <input name="correo_part" class="input-block-level text-center" type="email" maxlength="30" placeholder="ejemplo@mail.com" title="Introduzca el correo del Participante">
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Apellidos:</span></div>
			<div class="span3">
			    <input name="apellido_part" class="input-block-level text-center" type="text" maxlength="39" placeholder="Apellidos del Participante" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{3,}" required>
			</div>
			<div class="span2"></div>
			<div class="span2"><span>Ocupación:</span></div>
			<div class="span3">
			    <select name="ocup_part" class="input-block-level" title='Seleccione la ocupación del participante' requerid>
				<option>Seleccione...</option>
				<option value="Estudiante">Estudiante</option>
				<option value="Profesional">Profesional</option>
			    </select>
			</div>
		    </div><br>
		    <div class="row-fluid">
			<div class="span2"><span>Género:</span></div>
			<div class="span7" title="Elija el Genero del Participante">
			    <input  type="radio" name="genero_part" value="Masculino" checked="checked"/> Masculino
			    <input  type="radio" name="genero_part" value="Femenino" /> Femenino
			</div>
		    </div><br>
		    <div class="row-fluid">
			<div class="span2"><span>Dirección:</span></div>
			<div class="span7">					    
			    <textarea name="direccion_part" class="input-block-level" cols="64" rows="5" placeholder="Dirección del Participante" title="Puede introducir letras, Simbolos y Números" required></textarea>
			</div>
		    </div>
	       </div>
	    </div>
	    <hr><br />
	    <div class="row-fluid">
	       <div class="span12 text-center">
		    <button type="submit" class="btn btn-primary"><i class="icon-hdd icon-white"></i> Registrar</button>
		    <button type="reset" class="btn btn-warning"><i class="icon-trash icon-white"></i> Limpiar</button>
	       </div>
	    </div>
	</form><!--cierre del formulario-->
     </div><!--cierre del Hero-Unit-->
</div><!--cierre del contenido-->
</div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<?php


}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permisos para Registrar los Participantes.');
                      document.location=('index.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
require('piepagina.php');
?>