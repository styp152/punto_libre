<?php
     require('cabecera.php');
     require('menu.php');
       $con= @mysql_connect("localhost","root","21183652");
     
     if(!mysql_select_db("administracion", $con)){
         echo "error al conectarse";}
         include("../../Sistema_administrativo/php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Acceso_Cursos']==1 and $_SESSION['Insertar_Informacion']==1){
     
     require('conexion.php');
?>
		<div class="span9">
		    <div class="hero-unit">
			<h3 class="text-center">Registro de Instructor</h3>
			<form method="post" action="instructor_insertar.php">
			    <div class="row-fluid">
				<div class="span12 text-center btn-primary">
				    <span>Datos del Instructor</span>
				</div>
			    </div>
			    <div class="row-fluid">
				<div class="span12"><br />
				    <div class="row-fluid">
					<div class="span2"><span>Cédula:</span></div>
					<div class="span3">
					    <input name="ci_instru" type="text" class="input-block-level  text-center" maxlength="10" placeholder="00000000" title="Solo debe introducir Números" pattern="[0-9]{0,}" required>
					</div>
					<div class="span2"></div>
					<div class="span2"><span>Teléfono:</span></div>
					<div class="span3">					    
					    <input name="tlfn_instru" class="input-block-level text-center" type="text" size="15" maxlength="12" placeholder="0000-0000000" title="Solo debe introducir números" pattern="[0-9]{4}[-][0-9]{7}" required>
					</div>
				   </div>
				    <div class="row-fluid">
					<div class="span2"><span>Nombres:</span></div>
					<div class="span3">
					    <input name="nombre_instru" class="input-block-level  text-center" type="text" maxlength="39" placeholder="Nombres del Instructor" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{3,}" required>
					</div>
					<div class="span2"></div>
					<div class="span2"><span>Correo:</span></div>
					<div class="span3">
					    <input name="correo_instru" class="input-block-level text-center" type="email" size="40" maxlength="30" placeholder="ejemplo@mail.com" title="Introduzca el correo del Participante">
					</div>
				    </div>
				    <div class="row-fluid">
					<div class="span2"><span>Apellidos:</span></div>
					<div class="span3">
					     <input name="apellido_instru" class="input-block-level  text-center" type="text" maxlength="39" placeholder="Apellidos del Instructor" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{0,}" required>
					</div>
					<div class="span2"></div>
					<div class="span2"><span>Ocupación:</span></div>
					<div class="span3">
					     <select name="ocupacion_instru" class="input-block-level" title='Seleccione la ocupación del instructor' requerid>
						  <option>Seleccione...</option>
						  <option value="Estudiante">Estudiante</option>
						  <option value="Profesional">Profesional</option>
					     </select>
					</div>
				   </div><br>
				   <div class="row-fluid">
					<div class="span2"><span>Género:</span></div>
					<div class="span4" title="Elija el Género del Instructor">
					     <input  type="radio" name="genero_instru" value="Masculino" checked="checked"/> Masculino
					     <input  type="radio" name="genero_instru" value="Femenino" /> Femenino
					</div>
				   </div><br>
				    <div class="row-fluid">
					<div class="span2"><span>Dirección:</span></div>
					<div class="span7">					    
					    <textarea name="direccion_instru" class="input-block-level" cols="64" rows="5" placeholder="Dirección del Instructor" title="Puede introducir letras, Simbolos y Números" required></textarea>
					</div>
				    </div>
			      </div>
			    </div>
			    <hr><br />
			    <div class="row-fluid">
				<div class="span12 text-center">
				   <button type="submit" class="btn btn-primary"><i class="icon-hdd icon-white"></i> Registrar</button>
				   <button type="reset" class="btn btn-warning"><i class="icon-trash icon-white"></i> Limpiar</button>
			      </div>
			    </div>
			</form>
		    </div>
	        </div>
	    </div>
	</div>
<?php


}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permisos para Registrar los Instructores de los cursos.');
                      document.location=('index.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
require('piepagina.php');
?>