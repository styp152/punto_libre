
<?php
 session_start();

         

  ?>
<!DOCTYPE html>
    <html lang="es">
    <head>
        <title>Sistema Administrativo - Gestión de Cursos "Punto Libre"</title>
	<meta name="autor" content="Miguel Sanabria, Germain Rojas, Yuberli Vielma" /> <!--Autores del Sistema-->
        <meta charset="UTF-8"><!-- codificación de caracteres es UTF-8-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" ><!--Se adapta el sistema en formato movil-->
	<link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all"/>
	<link rel="stylesheet" href="../css/bootstrap-responsive.css" media="screen"/>
	<link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" media="screen"/>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
	
    </head>
    <body>
    <div class="wrapper">
	<div class="navbar navbar-fixed-top"><!--Barra Superior Azul-->
	    <div class="navbar-inner">
		<div class="container-fluid">
		    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		    </button>
		    <a class="brand" href="#"><img src="../img/PuntoLibreLogo.svg" width='20' height='20'/> Punto Libre </a>
		    <div class="nav-collapse collapse">
			<ul class="nav">
			    <li><a href="../../Sistema_administrativo/html/menu_definitivo.php"><i class="icon-home"></i> Administración de P.L.</a></li>
			    <li class="active dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-hdd"></i> Gestion de Cursos<b class="caret"></b></a>
				<ul class="dropdown-menu">
				    <li><a href="curso_registro.php" ><i class="icon-pencil"></i>Registro de Cursos</a></li>
				    <li><a href="curso_consultar.php"><i class="icon-search "></i>Consulta de Cursos</a></li>
				    <li><a href="participante_consultar.php"><i class="icon-list "></i>Consulta de Participantes</a></li>
				    <li><a href="instructor_consultar.php"><i class="icon-list "></i>Consulta de Instructores</a></li>
				</ul>
			    </li>
			    <li><a href="../../inventario/html/index.php"><i class="icon-book"></i> Inventario </a></li>
			    <li><a href="../../gs/html/cliente.php"><i class="icon-tasks"></i> Soporte a Equipos </a></li>
			</ul>
			<div class="pull-right">
			    <ul class="nav pull-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i><?php echo  $_SESSION['usuario']; ?><b class="caret"></b></a>
				    <ul class="dropdown-menu">
					<li><a href="#"><i class="icon-user"></i>Perfil</a></li>
					<li><a href="#"><i class="icon-headphones"></i>Ayuda General</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-off"></i>Cerrar sesión</a></li>
				    </ul>
				</li>
			    </ul>
			</div>
		    </div><!--Cierre del nav-collapse-->
		</div><!--Cierre del Container-->
	    </div><!--Cierre del Navbar-inner-->
	</div><!--Cierre de la barra superior-->
	<br />
	<br>