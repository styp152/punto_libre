<?php
  session_start();
  include("../conf.php");
  include("../php/lib/conexion.php");
  $con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Insertar_Informacion']==1){
    
    $db=$_CONF['db_mysql'];
    $sql="SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES
         WHERE TABLE_SCHEMA = '$db' AND   TABLE_NAME   = 'reporte_fallas';";
  
  $resp=mysql_query($sql,$con);
  if(!$resp){
      echo "Error hubo un problema";
      echo mysql_error;
      exit();
  }else{
    $id=null;  
    while($fila=mysql_fetch_assoc($resp)){
      
      $id=$fila['AUTO_INCREMENT'];
      
    }
    
  
  }
  
  ?>
  






<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Reporte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />
  </head>

  <body>
    
<form  action="<?php echo $_CONF['server_web'].$_CONF['app']."php/reportes.php"; ?>" method="post"> 

      <?php
        include("cabecera.php")
      ?>             
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav  affix"">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active" title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
              
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li><a href="../php/respaldo.php">Respaldar la Base de Datos </a></li>
                                <li><a href="../php/respaldo.php">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>


  <div class="span9">
    <div class="hero-unit">

			  <br>
			  <br>
		 <h3 class="text-center">Sistema Administrativo</h3>
		 <div class="row-fluid">
          <div class="span12 text-center btn-primary  ">
                <span>Reporte de inconvenientes</span>
         </div>
         </div>
		 <br>
		 
		 
		 
		  <table id="tabla">
                <div class="row-fluid">
                <div class="span12 text-center">
                                                                                                              

		<div class="span4">
		<span>Codigo del reporte</span>
		<input type="text" id= "codigo" name="codigo"  value="<?php echo $id; ?>" class="text-right" readonly />
		</div>


	<div class="span4">
	<span>Fecha del Reporte</span>
		 <div class="control-group">
		<div class="controls input-append date form_date" data-date="" data-date-format="yyyy-mm-dd" 
		  data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
		<input size="16" type="text" name="fecha__creacion" value="" readonly required>
		 <span class="add-on"><i class="icon icon-calendar"></i></span>
		</div>
		 <input type="hidden" id="dtp_input2" value="" /><br/>
								 </div>
							    </div>	 
  
  
		<div class="span4">
		<span>Tipo de problema</span>
		<input type="text" id="problema" name="problema" required>
		</div>

						
									
																				
		  <div class="row-fluid">
		  <div class="span12 text-center">								
									  
	    <div class="span12">
			  <br/>				
	    <span> Descripción del problema </span><br>
	    <textarea id="Descripcion" name="Descripcion" maxlength="150" cols="40" rows="3" required></textarea>
																	  
		  </div>                            
			  </div>
			   </div>
			  <br><br>
		  
                <div class="row-fluid">
                <div class="span12 text-center">
                  <div class="btn-group">
                 
   
    
          <button type="submit" class="btn btn-primary" onclick=" this.disabled=true;" ><i class="icon-file icon-white"></i> Enviar</button>
 


                   <button type="reset" title="Se cancelara la acción"class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
                   </div>
                </div>
          


     
      <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>

	<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
		<!--<script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>-->
		 
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script><!-- Script y js del calendario -->
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script> <!-- Script y js del calendario en español -->
    <script type="text/javascript">
	$('.form_date').datetimepicker
	({
	   language:  'es',
	   weekStart: 1,
	   todayBtn:  1,
		   autoclose: 1,
		   todayHighlight: 1,
		   startView: 2,
		   minView: 2,
		   forceParse: 0
       });
	   $('.form_time').datetimepicker
	   ({
	   language:  'es',
	   weekStart: 1,
	   todayBtn:  1,
		   autoclose: 1,
		   todayHighlight: 1,
		   startView: 1,
		   minView: 0,
		   maxView: 1,
		   forceParse: 0
       });
	</script>
                
</form>
</body>
</html>
<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
 
  
}
?>







