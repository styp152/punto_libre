<?php
session_start();
include("../conf.php");
include("../php/lib/conexion.php");
$con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Modificar_Informacion']==1){
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>modificacion de los puntos libres</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />

  </head>

  <body>
      <?php
        include("cabecera.php")
      ?>
           <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav  affix"">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active" title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
              
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li><a href="../php/respaldo.php">Respaldar la Base de Datos </a></li>
                                <li><a href="../php/respaldo.php">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
							
                       
                        
                               <?php
	     
	     $_CONSUL=array();
	      
	      $id_punto=$_POST['idpunto_libre'];
	  
	       $consul= "SELECT * FROM punto_libre  WHERE (idpunto_libre='$id_punto')";
	       $resp=mysql_query($consul);
	       if(!$resp){
		  //Error en la consulta
		  echo "Error en la consulta: ".mysql_error();
	       }else if(mysql_numrows($resp)==0){
		  
		  //No coincide el usuario y la contraseña
		//  $_CONSUL['error']="Cedula no encontrada en la base de datos";
		 
		// header("Location:". $_CONF['server_web'].$_CONF['app']."html/consultausuarios.php");
		   echo "<script type=text/javascript>
				    alert(' Punto no existente en la base de datos. Presiona aceptar para elegir otro codigo de punto');
				    document.location=('../html/modificacion_de_los_puntos.php');
				</script>";
	    }else{
		    
		    
		    $registro=mysql_fetch_assoc($resp);
		    $_CONSUL['idpunto_libre']=True;
		    if($_CONSUL['idpunto_libre']==True)
		    
		    $_CONSUL['idpunto_libre']=$id_punto;
           

						
		    $sql=" SELECT *
		FROM `usuarios_has_punto_libre`
		WHERE punto_libre_idpunto_libre ='$id_punto'";
		$resp=mysql_query($sql);
		
		if(!$resp){
			
			$error="Lo sentimos, hubo un error" . mysql_error();
			
		}else{
			$array_salida=array();
			$i=0;
			while($fila=mysql_fetch_assoc($resp)){
				$array_salida[$i]["punto_libre_idpunto_libre"]=$fila["punto_libre_idpunto_libre"];
				$array_salida[$i]["usuarios_idusuarios"]=$fila["usuarios_idusuarios"];
				$array_salida[$i]["Tipo_cargo"]=$fila["Tipo_cargo"];
			       
				$i++;
			}
			
		}		    
									
			    }
	    
			    $sql=" SELECT usuarios.idusuarios, personas.nombres, personas.apellidos, personas.cedula, personas.email FROM  usuarios JOIN personas ON usuarios.personas_idpersonas=personas.idpersonas
			    WHERE usuarios.idusuarios";
		      $resp=mysql_query($sql);
		      
		      if(!$resp){
			      
			      $error="Lo sentimos, hubo un error" . mysql_error();
			      
		      }else{
			      $array_salida1=array();
			      $i=0;
			      while($fila1=mysql_fetch_assoc($resp)){
		      
				      $array_salida1[$i]["nombres"]=$fila1["nombres"];
				      $array_salida1[$i]["apellidos"]=$fila1["apellidos"];
				      $array_salida1[$i]["cedula"]=$fila1["cedula"];
				      $array_salida1[$i]["email"]=$fila1["email"];
				      $array_salida1[$i]["idusuarios"]=$fila1["idusuarios"];
				     
				      $i++;
			      }
			      
		      }	
                                                          
                                                      
							?>
							<div class="span9">
		
                       <div class="hero-unit">
                        
                                  <H3 class="text-center btn-primary">Planilla para Modificar el cargo del Punto Libre</H3>
                                  <br>
                                    <br>
                                  <table class='table table-striped'class='span12'  >
                                    <tr>
                                        <td>Id Punto libre</td>
					<td>Codigo de usuario</td>
                                        <td>Tipo de Cargo</td>
				    	<?php
        
					//include("../php/consulta_general.php");
					if (isset($error)) echo $error;
					    for($i=0; $i<count($array_salida);$i++){ ?>
                                    </tr>
				        <td><?php echo $array_salida[$i]["punto_libre_idpunto_libre"]; ?></td>
                                        <td><?php echo $array_salida[$i]["usuarios_idusuarios"]; ?></td>
                                        <td><?php echo $array_salida[$i]["Tipo_cargo"]; ?></td>
                                    </tr>
				    
                                     <?php } ?>
				     <tr><td>Para modificar el cargo de este punto:</td>
				      <td></td>
      
				         <td> <button class="btn btn-danger" href="#myModal1" data-toggle="modal" role="button"><i class="icon-file icon-white"></i> Modificar</button></td>
				    </tr>
                                   </table>
				  <hr class="btn-primary">
				  <br>  
				  <br>
				  <h4  class="text-center btn-primary">En la siguiente tablas encontraras todos los usuarios inscritos en el sistema con su descripción</h4>
				   <br>
				    <br>
				   <table width="40%" class="table table-striped" border=4 bordercolor="#0080FF" cellpadding=2	align="center">
									<tr  VALIGN=top>
						        <th><h5>Codigo del usuario<h5></th>     
                                                        <th><h6> Nombres</h6></th>
                                                        <th><h6> Apellidos</h6></th>
                                                         <th><h5> Cedula</h5></th>
                                                         <th><h5> Email</h5></th>
							

							</tr>
        
							<?php
        
                                                        
                                                        if (isset($error)) echo $error;
                                                            for($i=0; $i<count($array_salida1);$i++){ ?>
							    
							<tr>
                                                                <td><h6><?php echo $array_salida1[$i]["idusuarios"]; ?></h6></td>
                                                                <td><h6><?php echo $array_salida1[$i]["nombres"]; ?></h6></td>
                                                                <td><h6><?php echo $array_salida1[$i]["apellidos"]; ?></h6></td>
                                                                <td><h6><?php echo $array_salida1[$i]["cedula"]; ?></h6></td>
                                                                <td><h6><?php echo $array_salida1[$i]["email"]; ?></h6></td>
                                                               
                
							
                                                             
                                                                </tr>
                                                                
					  	<tr>
                                               
						  <?php } ?>		    
						</table>		  
                       </div>
		
                </div>
		                <div id="myModal1" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">Introduce el Codigo del usuario a modificar</p>
                                  </div>
                                   <div class="modal-body">
                                                Codigo del usuario:
                                   </div>
                                    <form method="post" action="modificar_cargo_2.php" class="form-search" >
                                           <div class="modal-footer">
                                             <input type="hidden" name="id_punto" value="<?php echo $id_punto;?>"/>
                                                    <input  type="text" name="id_usuarios" class="input-medium search-query">
                                                    <input type="submit"  class="btn btn-primary">
                                          </div>
                      
             </div>
        </div>
	                           <script type="text/javascript" src="../js/jquery.js"></script>
                                   <script type="text/javascript" src="../js/bootstrap.js"></script>
                                   <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
                                  
                                 
                                

  
                                 
                            </script>

                        
                          
                          <script type="text/javascript">
                              $('#myModal1').on('hidden', function ()
                                                
                                                
                              // do something…
                            
                            
<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>
