
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>modificacion de los puntos libres</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />

  </head>

  <body>
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li ><a href="menu_definitivo.html"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown active"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i> Administración de P.L.<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="permisologia.HTML"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="consulta_de_usuarios.HTML"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="consulta_de_usuarios.HTML"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="reporte.HTML"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="#"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="menu_crear_punto.HTML"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                           <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar y Deshabilitar los Puntos Libres</a></li>
                         
                        
                          <li><a href="ayuda.html"><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li  class="dropdown "><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Usuario <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="perfil.html"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.php" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>                                                
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav  affix"">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active" title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
              
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li><a href="../php/respaldo.php">Respaldar la Base de Datos </a></li>
                                <li><a href="../php/respaldo.php">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
							                    </div>
							<?php

							include("conexion_administracion.php");

							$codigoP=$_REQUEST['idpunto_libre'];

							$sql="select * from punto_libre where idpunto_libre='$codigoP'";

							$result =mysql_query($sql);

							$row =mysql_fetch_assoc($result); //en la variable roww s va almacenar todo en forma de arreglo..   

							//el readonly es para no cambiar el dato almacenado, ejemplo codigo.

							?>



							         <div class="span9">
							            <div class="hero-unit">
									       <h3 class="text-center">Planilla para Modificar un Punto Libre</h3>
									        <table id="tabla">
									        <form  action="modificar2.php" method="post"> 

											  
												  
											  <div class="row-fluid">
												  <div  class="span12 text-center btn-primary  ">
														<span>Ubicacion de nuevo punto libre</span></div>
												 </div>
												<div class="row-fluid">
												  <div class="span12">
															<div class="span3">
																<span>Codigo Punto</span>
																<input type="text" name="codigoP" readonly="readonly" value="<?php echo $row['idpunto_libre']?>"/>
															</div>	
															<div class="span3">  
																												
																	<div id="contenido-demo">	
																		 <span> Seleccionar Fecha:</span>
																		<input type="text" name="fecha_creacion" id="datepicker" size="12" value="<?php echo $row['fecha_creacion']?>"/>		
									                                </div> 
							                                </div>
													        <div class="span3 ">
															   <span>Estado:</span>
															   <br>
															   <input readonly="readonly" type="text" name="estado" size="12" value="<?php echo $row['estado']?>"/>
													        </div>
														   <div class="span3">
														    		<span>Ingrese Nuevo estado:</span>
														              <br>
														  
																   <select  name="estado">	
																		   <option value="">Seleccionar</option>}			
																		    <option value="Amazonas">Amazonas</option>
																			<optio value="Anzoategui">Anzoategui</option>
																			<option value="Apure">Apure</option>
																			<option value="Aragua">Aragua</option>
																			<option value="Barinas">Barinas</option>
																			<option value="Bolívar">Bolívar	</option>
																			<option value="Carabobo">Carabobo</option>
																			<option value="Cojedes">Cojedes</option>
																			<option value="Delta Amacuro">Delta Amacuro</option>
																			<option value="Distrito Capital">Distrito Capital</option>
																			<option value="Guárico">Guárico	</option>
																			<option value="Lara">Lara</option>
																			<option value="Merida">Mérida</option>
																			<option value="Miranda">Miranda</option>
																			<option value="Monagas">Monagas</option>
																			<option value="Portuguesa">Portuguesa	</option>
																			<option value="Sucre">Sucre</option>
																			<option value="Tachira">Táchira	</option>
																			<option value="Trujillo">Trujillo</option>
																			<option  value="Vargas">Vargas</option>
																			<option  value="Yaracuy">Yaracuy	</option>
																			<option  value="Zulia">Zulia</option>
																			<option>......</option>
																  </select>				
												           </div>
											      </div>
											   </div>
													<br>			

						                                                                       <div class="row-fluid">
																	<div class="span12">
																			<div class="span3 ">
																	  			 <span>Ciudad:</span>
																	  					 <br>
																	   			 <input readonly="readonly" type="text" name="ciudad" size="12" value="<?php echo $row['ciudad']?>"/>
															                </div>

																			       <div class="span4">
																				       <span>Ciudad:</span><br><select name="Ciudad">
																							<option>Seleccionar</option>
																							<option value="Caracas">Caracas</option>
																							<option value="Maracaibo">Maracaibo</option>
																							<option value="Valencia">Valencia</option>
																							<option value="Apure">Apure</option>
																							<option value="Aragua">Aragua</option>
																							<option value="Barquisimeto">Barquisimeto</option>
																							<option value="San Cristóbal">San Cristóbal	</option>
																							<option value="Ciudad Guayana">Ciudad Guayana</option>
																							<option value="Barcelona">Barcelona</option>
																							<option value="Valera">Valera</option>
																							<option value="Maracay">Maracay	</option>
																							<option value="Petare">Petare	</option>
																							<option value="Turmero">Turmero</option>
																							<option value="Ciudad Bolívar">Ciudad Bolívar</option>
																							<option value="Barinas">Barinas</option>
																							<option value="Santa Teresa del Tuy">Santa Teresa del Tuy</option>
																							<option value="Cumaná">Cumana</option>
																							<option value="Baruta">Baruta</option>
																							<option value="Puerto la Cruz">Puerto la Cruz	</option>
																							<option value=">Mérida">Mérida</option>
																							<option value="Cabimas">Cabimas</option>
																							<option value="Coro">Coro</option>
																							<option value="Guatire">Guatire</option>
																							<option value="Cúa">Cúa	</option>
																							<option value="Guarenas">Guarenas</option>
																							<option value="Los Teques">Los Teques</option>
																							<option value="Ocumare del Tuy">Ocumare del Tuy</option>
																							<option value="Puerto Cabello">Puerto Cabello</option>
																							<option value="Guacara">Guacara</option>
																							<option value="El Tigre">El Tigre</option>
																							<option value="El Limón">El Limón</option>
																							<option value="Acarigua">Acarigua	</option>
																							<option value="Punto Fijo">Punto Fijo</option>
																							<option value="Cabudare">Cabudare</option>
																							<option value="Charallave">Charallave</option>
																							<option value="Palo Negro">Palo Negro</option>
																							<option value="Cagua">Cagua</option>
																							<option value="Anaco">Anaco</option>
																							<option value="Calabozo">Calabozo</option>
																							<option value="Guanare">Guanare</option>
																							<option value="Carúpano">Carúpano</option>
																							<option value="Ejido">Ejido</option>
																							<option value="Catia La Mar">Catia La Mar</option>
																							<option value="Mariara">Mariara</option>
																							<option value="">......</option>
																					
																							</select>	
															                    	</div>
											       
			                                              				 <div class="span4 ">
				 								
																				<span>Direccion</span>
																	            <textarea placeholder="indique la direccion con exactitud" name="direccion" maxlength="150" cols="40" rows="3" value="<?php echo $row['direccion']?>"/> </textarea>
																													
																						    	
									                             	     </div> 
									                             	</div> 
									                           </div>
																<div class="row-fluid">
																       <div class="span12">
																           <div class="span12">
                                                                                                                                              <span>    Croquis:  </span>     
                                                                                                                                                                                                                                                                                                                                                                      
                                                                                                                                              <p class="muted">Carga el croqui del Nuevo Punto:
                                                                                                                                              <colspan="2"><input type="file" name="imagenes">
                                                                                                                                              
															                 </div>
															           </div> 
														        </div>
																	<div class="row-fluid">
																			<div class="span12 text-center btn-primary">
																					<span>Datos del Encargado</span>
																			</div>
																	</div>
																	<div class="row-fluid">
																			<div class="span12">
																					<div class="span3">
																						<span>Nombre:</span> <input type="text" name="nombre" value="<?php echo $row['nombre']?>"/> 
																					</div>
																					<div class="span3">
																							<span>Apellido:</span> <input type="text" name="apellido" value="<?php echo $row['apellido']?>"/> 
																					</div>
																					<div class="span3">
																						<span>Teléfono:</span>  <input type="text" name="telefono" value="<?php echo $row['telefono']?>"/> 
									 
																					</div>
															 				 </div>
															 		</div>

																	 <div class="row-fluid">
																	<div class="span12">

																	<div class="span3">
																	<span>Correo Electronico</span>  <input type="text" name="correo_electronico" value="<?php echo $row['correo_electronico']?>"/> 
																	</div>
																	<div class="span3">
																	<span>Cédula:</span> <input type="text" name="cedula" value="<?php echo $row['cedula']?>"/> 
																	 </div>

																	</div>
															  </div>
											    


																	<div class="row-fluid">
																			<div class="span12 text-center btn-primary">
																				<span>Inventario</span></div>
																			</div>

											                                 <table>	
									                                              <tr>									
								                                                  	<td align="right"><class="muted"> <span>¿ Desea cargar el inventario en este momento?</span></td>
																					
																			   	</tr>
								                                            	<tr>

																					<td align="right"><p class="muted">Cargar inventario </td>
																					<td><input type="radio" name="opcion" value="cargar inventario" checked>   </td>

																							<td align="right"><p class="muted">En otro momento
																							<td><input type="radio" name="opcion" value="en otro momento" > </td>
																				</tr>
											                                    <br><br>

															      
															
															                 </table>
																				<br>
																				<br>

																	  <div class="row-fluid">
															                <div class="span12 text-center">
															                       <div class="btn-group">
															               
																	                   <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
																	                   <button type="submit" class="btn btn-primary" ><i class="icon-file icon-white"></i> Modificar</button>

															                        </div>
															                </div>
															          </div>
															        </div>

										
									        
									           </div>
									        </div>

									      </div> 
									  </div>
			         
			        <script type="text/javascript" src="../js/bootstrap.js"></script>
			        <script type="text/javascript" src="../js/jquery.js"></script>
			       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
				<script type="text/javascript">
				${"dropdown-toggle"}.dropdown{}
				       
				  </script>
				
				  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
				<script type="text/javascript">
				    $('#myModal').on('hidden', function () {
				    // do something…
				    })</script>
				
					<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
					<script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>
					<script type="text/javascript">
				jQuery(function($){
					$.datepicker.regional['es'] = {
						closeText: 'Cerrar',
						prevText: '&#x3c;Ant',
						nextText: 'Sig&#x3e;',
						currentText: 'Hoy',
						monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
						'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
						monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
						'Jul','Ago','Sep','Oct','Nov','Dic'],
						dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
						dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
						dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
						weekHeader: 'Sm',
						dateFormat: 'dd/mm/yy',
						firstDay: 1,
						isRTL: false,
						showMonthAfterYear: false,
						yearSuffix: ''};
					$.datepicker.setDefaults($.datepicker.regional['es']);
				});    
				
					$(document).ready(function() {
					   $("#datepicker").datepicker();
					});
				    </script>
			 
			</form>
	</body>
</html>


