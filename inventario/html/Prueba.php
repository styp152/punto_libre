<?php
require('../fpdf/fpdf.php');

class PDF extends FPDF
{
	
function Header()
		{
			
       $this->Image('../img/PuntoLibreLogo.jpg',15,10,28);
       $this->Ln(10);
       $this->SetFont('Helvetica','B',16);
       $this->SetTextColor(185,20,20);
       $this->Cell(88,5,'GNU de Venezuela',0,0,'R');
       $this->SetFont('Helvetica','B',14);
       $this->Ln();
       $this->Cell(85,5,'Punto Libre Merida ',0,0,'R');
       $this->Ln(25);
      }
	
	function Footer()
		{
   	 $this->SetY(-15);
    	 $this->SetFont('Arial','I',8);
       $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
		}	
	
	
	
	
// Cargar los datos
function LoadData($file)
{
    // Leer las líneas del fichero
    $lines = file($file);
    $data = array();
    foreach($lines as $line)
        $data[] = explode(';',trim($line));
    return $data;
}

// Tabla coloreada
function FancyTable($header, $data)
{
    
  	mysql_connect('localhost','root','0000');
	mysql_select_db('inventario');

    $this->SetFont('Arial','B','15');
    $this->Cell(190,10,'INVENTARIO DE BIENES MUEBLES',0,0,'C');
    $this->Ln(20);
    // Colores, ancho de línea y fuente en negrita
    $this->SetFillColor(200,220,250);
    $this->SetTextColor(10);
    $this->SetDrawColor(00,00,00);
    $this->SetLineWidth(.1);
    $this->SetFont('','');
    // Cabecera

    $this->SetFont('','');
    $this->SetFillColor(200,220,250);
    $this->SetFont('Arial','B',14);
    $this->Cell(190,10,'MOBILIARIO INCORPORADO',1,1,'C');

           
    $w = array(50, 60, 30, 50);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        
    // Restauración de colores y fuentes
    $this->SetFillColor(224,235,355);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Datos
    $fill = false;
    $SQL="SELECT * FROM recepcion,bien,mobiliario WHERE recepcion.cod_registro=bien.cod_registro and  bien.cod_bien=mobiliario.cod_bien and bien.status='Incorporado'";  
    $Ejecucion=mysql_query($SQL);
    while($res=mysql_fetch_array($Ejecucion))
   {
   $this->Cell(50,15,$res['cod_bien'],1,0,'C');
   $this->Cell(60,15,$res['fecha_registro'],1,0,'C');
   $this->Cell(30,15,$res['serial_mobiliario'],1,0,'C');    
   $this->Cell(50,15,$res['descripcion'],1,0,'C'); 
   $this->Ln();
   }
    
    foreach($data as $row)
    {
        $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
        $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
        $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
        $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
        $this->Ln();
        $fill = !$fill;
    }
    // Línea de cierre
    $this->Cell(array_sum($w),0,'','T');
}
}

$pdf = new PDF();
// Títulos de las columnas  <meta charset="UTF-8">
$header = array('Codigo', 'Fecha Registro', 'Serial', 'Descripcion');
// Carga de datos
$data = $pdf->LoadData('paises.txt');
$pdf->SetFont('Arial','',13);


$pdf->AddPage();
$pdf->FancyTable($header,$data);
$pdf->Output();
?>
