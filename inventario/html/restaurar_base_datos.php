<?php
require("cabecera.php");
require("menu.php");
require('conexion.php');
?>
<div class='span9'>
	<div class='hero-unit'>
		<div class='row-fluid'>
			<div class='span12 text-center btn-primary'>
			    <span>Restaurar Base de Datos</span>
			</div>
		</div><br>
		<div class='row-fluid'>
			<div class="span12 text-center">
			    <span>Restaurar base de datos de un archivo de respaldo guardado</span>
			</div>
		</div><br>
		<div class="row-fluid">
			<div class="span12 text-center">
				<form action='respaldar.php?act=aplicarresp' method='post' enctype='multipart/form-data'>
					<input name='archivo' type='file' size='20'/>
					<input name='enviar' type='submit' title='restaura base de datos del sistema' id='boton' value='Restaurar' />
					<input name='action' type='hidden' value='upload' />     
				</form>
			</div>
		</div><hr>
	</div>
</div>
</div>
</div>
<?php
require("piepagina.php");
?>