
    <div class="container-fluid">
      <div class="row-fluid">
	<div class="span3">
	  <div class="well affix">
	    <ul class="nav nav-pills nav-stacked">
	      <li class="nav-hearder"><h4>Control de Inventario</h4></li>
	      
		<li><a href="planilla_recepcion.php"><i class="icon-pencil"></i> Registro de Bienes</a></li>
	      <li class="dropdown ">
			<a class="dropdown-toggle" data-toggle="dropdown" href=""><i class="icon-search"></i>Consulta de Bienes<b class="caret"></b></a> 
			<ul class="dropdown-menu">
			   <li><a href="planilla7_porTipo.php"><i class="icon-search"></i> Consulta por Tipo</a></li>
		           <li><a href="planilla8_estatus.php"><i class="icon-search"></i> Consulta por Estatus</a></li>
			   <li><a href="consultaGeneral.php"><i class="icon-search"></i> Consulta General</a></li>
			</ul>	
	          </li>
	      <li class="dropdown ">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
		<ul class="dropdown-menu ">
		  <li><a href="respaldo_base_datos.php">Respaldar</a></li>
		  <li><a href="restaurar_base_datos.php">Restaurar</a></li>
		  <li class="divider"></li>
		  <li><a href="#"><i class="icon-headphones"></i>Ayuda</a></li>
		</ul>	
	      </li>
	    </ul>
	  </div>
	</div>
 
