<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');


$codigo = $_GET['cod_bien'];
$sql = "SELECT * FROM recepcion,bien,pc WHERE recepcion.cod_registro=bien.cod_registro and bien.cod_bien = pc.cod_bien and bien.cod_bien = '$codigo' LIMIT 0,1";
$result = mysql_query($sql);
$row = mysql_fetch_assoc($result);
?>
<div class="span9">
  <div class="hero-unit">
    <form method="post" action="modificarpc.php">
      <div class="row-fluid">
	      <div class="span12 text-center btn-primary">
		  <span><h4>Descripción del Bien</h4></span>
	      </div>
	    </div><br />
	      <div class="row-fluid">
		<div class="span4"">
		<strong>Nro de Registro:</strong>
		</div>
		<div class="span1">
		  <?=$row['cod_registro']?>
		</div>
	      </div>
	      <div class="row-fluid">
		<div class="span4">
		  <strong>Fecha de Registro:</strong>
		</div>
		<div class="span4">
		  <?=$row['fecha_registro']?>
		</div>
	      </div><hr>
	      <div class="row-fluid">
		<div class="span3">
		  <strong>Estado:</strong>
		  <?=$row['estado']?>
		</div>
		<div class="span4">
		  <strong>Municipio:</strong>
		  <?=$row['municipio']?>
		</div>
		<div class="span5">
		  <strong>Sede:</strong>
		    <?=$row['sede']?>
		</div>
	      </div><hr>
	      <div class="row-fluid">
	      <div class="span4">
		<strong>Código del Bien:</strong>
	      </div>
	      <div class="span2">
		<?=$row['cod_bien']?>
	      </div>
	      </div>
	    <div class="row-fluid">
	      <div class="span4">
		<strong>Fecha de Adquisición:</strong>
	      </div>
	      <div class="span3">
		<?=$row['fecha_adq']?>
	      </div>
	    </div>
	    <div class="row-fluid">
	      <div class="span4">
		<strong>Descripción del Bien:</strong>
	      </div>
	      <div class="span8">
		<?=$row['descripcion']?>
	      </div>
	    </div>
	   <div class="row-fluid">
	      <div class="span4">
	        <strong>Forma de Adquisición:</strong>
	     </div>
	     <div class="span2">
	      <?=$row['forma_adq']?>
	     </div>
	   </div>
	   <div class="row-fluid">
	      <div class="span4">
		  <strong>Proveedor:</strong>
	      </div>
	      <div class="span2">
		<?=$row['proveedor']?>
	      </div>
	    </div>
	    <div class="row-fluid">
	      <div class="span4">
		  <strong>Precio (IVA Incluido):</strong>
	      </div>
	       <div class="span4">
	        <span>Bs F </span> <?=$row['precio']?><span>.00</span>
	      </div>     
	    </div><br />
            <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span><h4>PC</h4></span>
		</div>
	      </div><br/>
	      <div class="row-fluid text-center">
		<div class="span2">
		  <strong>Marca:</strong>
		 <br> <?=$row['marca']?>
		</div>
		<div class="span2">
		  <strong>Serial:</strong>
		  <br><?=$row['serial']?>
		</div>
		<div class="span2">
		  <strong>DD:</strong>
		 <br><?=$row['dd']?>
		</div>
		<div class="span2">
		  <strong>RAM:</strong>
		 <br><?=$row['ram']?>
		</div>
		<div class="span2">
		  <strong>Placa:</strong>
		 <br><?=$row['placa']?>
		</div>
		<div class="span2">
		  <strong>CD / DVD</strong><br>
		 <?=$row['unidad']?>
		</div>
	      </div><br>
	     <div class="row-fluid">
		<div class="span6 text-center btn-primary">
			<span><h4>Monitor</h4></span>
		</div>
		<div class="span6 text-center btn-primary">
			<span><h4>Procesador</h4></span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span2">
		 <strong> Marca:</strong><br>
		<?=$row['marca_monitor']?>
		</div>
		<div class="span2">
		   <strong>Color:</strong><br>
		   <?=$row['color_monitor']?>
		</div>
		<div class="span2">
		    <strong>Pulgada:</strong>
		   <br><?=$row['pulgada']?>
		</div>
		<div class="span2">
		   <strong>Marca:</strong><br>
		   <?=$row['marca_procesador']?>
		</div>
		<div class="span2">
		   <strong>Tipo:</strong>
		   <br><?=$row['tipo']?>
		</div>
		<div class="span2">
		   <strong>GHZ:</strong>
		   <br><?=$row['ghz']?>
		</div>
	      </div><br>
	      <div class="row-fluid">
		<div class="span6 text-center btn-primary">
			<span><h4>Mouse</h4></span>
		</div>
		<div class="span6 text-center btn-primary">
			<span><h4>Teclado</h4></span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span2"></div>
		<div class="span1">
		   <strong>Color:</strong>
		   <?=$row['color_mouse']?>
		</div>
		<div class="span2">
		   <strong>Tipo:</strong>
		    <br><?=$row['tipo_mouse']?>
		</div>
		<div class="span3"></div>
		<div class="span1">
		   <strong>Color:</strong>
		   <?=$row['color_teclado']?>
		</div>
		<div class="span2">
		  <strong> Marca:</strong>
		 <br> <?=$row['marca_teclado']?>
		</div>
	      </div><hr>
	      <div class="row-fluid text-center">
		<div class="span12">
		  <strong>Observación:</strong>
		  <?=$row['observacion']?>
		</div>
	      </div>
	      <hr><br />
	      <div class="row-fluid">
		<div class="span12 text-center">
		    <a href="modificarpc.php?cod_bien=<?=$row['cod_bien']?>" class="btn btn-info"><i class="icon-pencil icon-white"></i> Modificar</a>
		    <a href="consultarpc.php" class="btn"><i class="icon-share-alt"></i> Atrás</a>
		</div>
	      </div>
	  </div>
	</div>
      </div>
    </div>
<?php
  require('piepagina.php');
?>
