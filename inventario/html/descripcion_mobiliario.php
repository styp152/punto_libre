<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');

$codigo = $_GET['cod_bien'];
$sql = "SELECT * FROM recepcion,bien,mobiliario WHERE bien.cod_bien =mobiliario.cod_bien and bien.cod_bien = '$codigo' LIMIT 0,1";

$result = mysql_query($sql);
$row = mysql_fetch_assoc($result);
?>
<div class="span9">
  <div class="hero-unit">
    <form method="post" action="modificarpc.php">
      <div class="row-fluid">
	      <div class="span12 text-center btn-primary">
		  <span><h4>Descripción del Bien</h4></span>
	      </div>
	    </div><br />
	      <div class="row-fluid">
		<div class="span4">
		<strong>Nro de Registro:</strong>
		</div>
		<div class="span1">
		  <?=$row['cod_registro']?>
		</div>
	      </div>
	      <div class="row-fluid">
		<div class="span4">
		  <strong>Fecha de Registro:</strong>
		</div>
		<div class="span4">
		  <?=$row['fecha_registro']?>
		</div>
	      </div><hr>
	      <div class="row-fluid">
		<div class="span3">
		  <strong>Estado:</strong>
		  <?=$row['estado']?>
		</div>
		<div class="span4">
		  <strong>Municipio:</strong>
		  <?=$row['municipio']?>
		</div>
		<div class="span5">
		  <strong>Sede:</strong>
		    <?=$row['sede']?>
		</div>
	      </div><hr>
	      <div class="row-fluid">
	      <div class="span4">
		<strong>Código del Bien:</strong>
	      </div>
	      <div class="span2">
		<?=$row['cod_bien']?>
	      </div>
	      </div>
	    <div class="row-fluid">
	      <div class="span4">
		<strong>Fecha de Adquisición:</strong>
	      </div>
	      <div class="span3">
		<?=$row['fecha_adq']?>
	      </div>
	    </div>
	    <div class="row-fluid">
	      <div class="span4">
		<strong>Descripción del Bien:</strong>
	      </div>
	      <div class="span8">
		<?=$row['descripcion']?>
	      </div>
	    </div>
	   <div class="row-fluid">
	      <div class="span4">
	        <strong>Forma de Adquisición:</strong>
	     </div>
	     <div class="span2">
	      <?=$row['forma_adq']?>
	     </div>
	   </div>
	   <div class="row-fluid">
	      <div class="span4">
		  <strong>Proveedor:</strong>
	      </div>
	      <div class="span2">
		<?=$row['proveedor']?>
	      </div>
	    </div>
	    <div class="row-fluid">
	      <div class="span4">
		  <strong>Precio (IVA Incluido):</strong>
	      </div>
	       <div class="span4">
	        <span>Bs F </span> <?=$row['precio']?><span>.00</span>
	      </div>     
	    </div><br />
	<div class="row-fluid">
	  <div class="span12 text-center btn-primary">
	      <span><h4>Mobiliario</h4></span>
	  </div>
	</div><br/>
	<div class="row-fluid text-center">
	  <div class="span3"></div>
	  <div class="span2">
	    <strong>Marca:</strong>
	    <?=$row['marca_mobiliario']?>
	  </div>
	  <div class="span2">
	    <strong>Modelo:</strong>
	    <?=$row['modelo_mobiliario']?>
	  </div>
	  <div class="span2">
	    <strong>Serial:</strong>
	   <br> <?=$row['serial_mobiliario']?>
	  </div>
	</div><br>
	<div class="row-fluid">
	  <div class="span12 text-center btn-primary">
	      <span><h4>Características</h4></span>
	  </div>
	</div><br/>
	<div class="row-fluid text-center">
	  <div class="span3"></div>
	  <div class="span2">
	    <strong>Material:</strong>
	    <?=$row['material_mobiliario']?>
	  </div>
	  <div class="span2">
	    <strong>Color:</strong>
	    <?=$row['color_mobiliario']?>
	  </div>
	  <div class="span2">
	     <strong>Dimensiones:</strong>
	     <?=$row['dimension_mobiliario']?>
	  </div>
	</div><hr>
	<div class="row-fluid text-center">
	    <div class="span12">
		<strong>Observaciones:</strong>
		<?=$row['observacion_mobiliario']?>
	    </div>
	  </div><hr><br />
	<div class="row-fluid">
	  <div class="span12 text-center">
	    <a href="modificarmobiliario.php?cod_bien=<?=$row['cod_bien']?>" class="btn btn-info"><i class="icon-pencil icon-white"></i> Modificar</a>
	    <a href="consultarmobiliario.php" class="btn"><i class="icon-share-alt"></i> Atrás</a>
	</div>	
	</div>  
    </form>
  </div>
 </div>
</div>
 </div>
 </div>
<?php
  require('piepagina.php');
?>
