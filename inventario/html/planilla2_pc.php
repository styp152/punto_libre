<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
?>
    <div class="span9">
      <div class="hero-unit">
	<h3 class="text-center">Equipos de Computación</h3>
	<form method="POST" action="insertar_pc.php">
	  <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
	      <span>Datos del Bien</span>
	    </div>
	  </div><br />
	  <div class="row-fluid">
	    <div class="span3">
	      Código del Bien:
	    </div>
	    <div class="span3">
	      <input type="text" class="text-center" name="cod_bien" placeholder="PC000" maxlength="6" title="Introducir Código de PC" pattern="[/PC]{2}[0-9]{3}" size="10" required/>
	    </div>
	    <div class="span2">
	      Fecha:
	    </div>
	    <div class="span3 control-group">
	      <div class="controls input-prepend date form_date" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd"
		   data-link-field="fecha_adq" data-link-format="yyyy-mm-dd">
		  <span class="add-on"><i class="icon icon-calendar"></i></span>
		  <input size="10" type="text" value="" required>
	      </div>
	      <input type="hidden" id="fecha_adq" name="fecha_adq" value="" /><br/>
	    </div>
	  </div>
	  <div class="row-fluid">
	     <div class="span3">
		Descripción del Bien:
	     </div>
	     <div class="span8">
		<textarea name="descripcion" class="input-block-level"  placeholder="Indique el nombre característico de bien" rows="5" cols="60" required></textarea>
	     </div>
	  </div>
	  <div class="row-fluid">
	    <div class="span3">
	       Adquisición:
	    </div>
	    <div class="span4">
	      Compra <input type="radio" name="forma_adq" value="Compra" onclick=" precio.disabled=false" checked/>
	      Donación <input type="radio" name="forma_adq" value="Donacion" onclick="precio.disabled=true"/>
	     </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		Proveedor:
	      </div>
	      <div class="span3">
		 <select name="proveedor" required title="Seleccionar el Proveedor">
		    <option value="">Seleccione</option>
		    <option value="CNTI">CNTI</option>
		    <option value="CANTV">CANTV</option>
		    <option value="Ministerio de Educación">Ministerio de Educación</option>
		    <option value="Vtelca">Vtelca</option>
		    <option value=" VIT"> VIT</option>
		 </select>
	      </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		  Precio (IVA Incluido):
	      </div>
	      <div class="span3 input-prepend input-append">
	        <span class="add-on">Bs F.</span>
		<input type="text" class="text-center" name="precio" size="8" maxlength="5" pattern="[0-9]{0,}" placeholder="000" required/>
		<span class="add-on">.00</span>
	      </div>     
	  </div><br>
	  <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
		<span>PC</span>
	    </div>
	  </div><br />
	  <div class="row-fluid text-center">
	    <div class="span2">
	      Marca:
	      <select name="marca" title="Seleccionar Marca del Case" required>
		    <option value="">Seleccione</option>
		    <option value="Hp">HP</option>
		    <option value="ACER">ACER</option>
		    <option value="IBM">IBM</option>
		    <option value="TOSHIBA">TOSHIBA</option>
		    <option value="Sivertec">SONY</option>
		    <option value="COMPAQ">COMPAQ</option>
		    <option value="LENOVO">LENOVO</option>
		    <option value="MSI">MSI</option>
		</select>
	    </div>
	    <div class="span2">
	      Serial:
	      <input type="text" name="serial" maxlenght="5" class="input-block-level text-center" pattern="[C/P]{1}[-][0-9]{3}[#]" title="Indicar la Incial del bien específico seguido del número de fabrica" placeholder="C-123# O P-123#"/>
	    </div>
	    <div class="span2">
	      Disco Duro:
	      <select name="dd" class="input-block-level" title="Seleccionar la Capacidad del Disco Duro" required>
		  <option value="">Seleccione</option>
		  <option value="80GB"> 80GB</option>
		  <option value="120GB"> 120GB</option>
		  <option value="160GB"> 160GB</option>
		  <option value="250GB"> 250GB</option>
		  <option value="320GB"> 320GB</option>
		  <option value="360GB"> 360GB</option>
		  <option value="500GB"> 500GB</option>
		  <option value="750GB"> 750GB</option>
		  <option value="1TB"> 1TB</option>
		  <option value="1.5TB"> 1.5TB</option>
		  <option value="2TB"> 2TB</option>
	      </select>
	    </div>
	    <div class="span2">
	      RAM:
	      <input type="text" name="ram" maxlenght="15" class="input-block-level text-center" placeholder="DDR 2GB" pattern="[A-Z]{0,4}[0-9]{0,2}[ ][0-9]{0,3}[GB-MB]{2}" title="Indica el tipo y capacidad de la Memoria RAM"/>
	    </div>
	    <div class="span2">
	      Placa:
	      <select name="placa" class="input-block-level" title="Seleccionar la Marca de la Tarjeta Madre" required>
		    <option value="">Seleccione</option>
		    <option value="BIOSTAR"> BIOSTAR</option>
		    <option value="INTEL"> INTEL</option>
		    <option value="ASUS"> ASUS</option>
		    <option value="GIGABYTE"> GIGABYTE</option>
		    <option value="FOXCONN"> FOXCONN</option>
		    <option value="MSI"> MSI</option>
		    <option value="ASRock"> ASRock</option>
		    <option value="PC CHIPS"> PC CHIPS</option>
	      </select>
	    </div>
	    <div class="span2">
	      CD / DVD
		<select name="unidad" required title="Seleccionar la opción deseada">
		    <option value="">Seleccione</option>
		    <option value="SI">SI</option>
		    <option value="NO">NO</option>
		    <option value="SOLO DVD">SOLO DVD</option>
		    <option value="SOLO CD">SOLO CD</option>
		</select>
	    </div>
	  </div><br>
	  <div class="row-fluid">
	     <div class="span6 text-center btn-primary">
		     <span>Monitor</span>
	     </div>
	     <div class="span6 text-center btn-primary">
		     <span>Procesador</span>
	     </div>
	   </div><br />
	   <div class="row-fluid text-center">
	     <div class="span2">
	       Marca:
		 <select name="marca_monitor" required title="Seleccionar Marca de Monitor">
		     <option value="">Seleccione</option>
		     <option value="VIT">VIT</option>
		     <option value="SHARP">SHARP</option>
		     <option value="SAMSUNG">SAMSUNG</option>
		     <option value="COMPAK">COMPAQ</option>
		     <option value="LG">LG</option>
		     <option value="SONY">SONY</option>
		     <option value="AOC">AOC</option>
		     <option value="PHILIPS">PHILIPS</option>
		     <option value="BENQ">BENQ</option>
		     <option value="SONY">ViewSonic</option>
		 </select>
	     </div>
	     <div class="span2">
		Color:
		   <select name="color_monitor" required title="Seleccionar Color de Monitor">
		     <option value="">Seleccione</option>
		     <option value="Negro">Negro</option>
		     <option value="Blanco">Blanco</option>
		     <option value="Gris">Gris</option>
		 </select>
	     </div>
	     <div class="span2">
		 Pulgada: <select name="pulgada" required title="Seleccionar Pulgada de Monitor">>
		     <option value="">Seleccione</option>
		     <option value="14">14"</option>
		     <option value="14.5">14.5"</option>
		     <option value="15">15"</option>
		     <option value="15.5">15.5"</option>
		     <option value="16">16"</option>
		     <option value="16.5">16.5"</option>
		     <option value="17">17"</option>
		     <option value="17.5">17.5"</option>
		     <option value="18">18"</option>
		     <option value="18.5">18.5"</option>
		     <option value="19">19"</option>
		     <option value="19.5">19.5"</option>
		     <option value="20">20"</option>
		     <option value="20.5">20.5"</option>
		 </select>
	     </div>
	     <div class="span2">
		Marca:
		<select name="marca_procesador" required title="Seleccionar Marca de Procesador">
		     <option value="">Seleccione</option>
		     <option value="INTEL">INTEL</option>
		     <option value="AMD">AMD</option>
		 </select>
	     </div>
	     <div class="span2">
		Tipo:
		<select name="tipo"  class="input-block-level" title="Seleccionar Tipo de Procesador" required>
		     <option value="">Seleccione</option>
		    <optgroup label="INTEL">
		     <option value="ATOM">ATOM</option>
		     <option value="CELERON">CÉLERON</option>
		     <option value="CORE2DUO">CORE2 DÚO</option>
		     <option value="PENTIUM">PENTIUM</option>
		     <option value="PENTIUMDUAL">PENTIUM DUAL CORE</option>
		     <option value="CORE I3">CORE I3</option>
		     <option value="CORE I5">CORE I5</option>
		     <option value="CORE I7">CORE I7</option>
		     <optgroup label="AMD">
		     <option value="ATHLON">ATHLON</option>
		     <option value="DURON">DURON</option>
		     <option value="SEMPRON">SEMPRON</option>
		     <option value="ATHOLON 64">ATHLON 64</option>
		     <option value="AHTLON 64 X2">ATHLON 64 X2</option>
		     <option value="AMD K10">AMD K10</option>
		     <option value="PHENOM">PHENOM</option>
		</select> 
	     </div>
	     <div class="span2">Velocidad:</div>
	      <div class="span2 input-append">
	       <input type="text" class="text-center" name="ghz" size="2" placeholder="3.8" pattern=[0-9]{1}[.][0-9]{1,2} title="Describir la velocidad de GHZ"/>
		<span class="add-on">GHZ</span>
	     </div>
	    </div><br>
	    <div class="row-fluid">
	      <div class="span6 text-center btn-primary">
		      <span>Mouse</span>
	      </div>
	      <div class="span6 text-center btn-primary">
		      <span>Teclado</span>
	      </div>
	    </div><br />
	    <div class="row-fluid text-center">
	      <div class="span1"></div>
	      <div class="span2">
		 Color:
		 <select name="color_mouse" required>
		      <option value="">Seleccione</option>
		      <option value="Negro">Negro</option>
		      <option value="Blanco">Blanco</option>
		      <option value="Gris">Gris</option>
		  </select>
	      </div>
	      <div class="span2">
		Tipo:
		  <select name="tipo_mouse" required>
		      <option value="">Seleccione</option>
		      <option value="Trackball">Trackball</option>
		      <option value="Óptico">Óptico</option>
		  </select>
	      </div>
	      <div class="row-fluid text-center">
	      <div class="span2"></div>
	      <div class="span2">
		Color:
		<select name="color_teclado" required>
		     <option value="">Seleccione</option>
		      <option value="Negro">Negro</option>
		      <option value="Blanco">Blanco</option>
		      <option value="Gris">Gris</option>
		  </select>
	      </div>
	      <div class="span2">
		 Marca:
		     <select name="marca_teclado"title="Seleccionar Marca de Teclado" required>
		      <option value="">Seleccione</option>
		      <option value="SIRAGOM">SIRAGON</option>
		      <option value="DELL">DELL</option>
		      <option value="HP">HP</option>
		      <option value="GENIUS">GENIUS</option>
		      <option value="LENOVO">LENOVO</option>
		      <option value="SONY">SONY</option>
		      <option value="MAXELL">MAXEL</option>
                       <option value="BENQ">BENQ</option>
		     </select>
	      </div>
	    </div><br><hr>
	    <div class="row-fluid text-center">
	      <div class="span12">
		  Observación:<textarea name="observacion" class="input-block-level" placeholder="Indique el tipo de obsevación que desea registrar del bien a incorporar " cols="70" rows="5"></textarea>
	      </div>
	    </div><hr><br />
	    <div class="row-fluid">
	      <div class="span12 text-center">
		<button type="submit" class="btn btn-primary"><i class="icon-hdd icon-white"></i> Registrar</button>
		<button type="reset" class="btn btn-warning"><i class="icon-trash icon-white"></i> Limpiar</button>
		<a class="btn btn-danger" href="#myModal" data-toggle="modal" role="button"><i class="icon-remove icon-white"></i> Cancelar</a>
	    </div>
	  </form>
	</div>
      </div>
    </div>
  </div></div>
<!-- ----------------------------------------------------------------------------------------------------- -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header ">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Advertencia de Seguridad</h3>
</div>
<div class="modal-body alert alert-block">
<strong><p>¿Esta seguro que desea cancelar el registro?</p></strong>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-share-alt"></i> Volver</button>
<button type="submit" class="btn btn-danger" onclick="location.href='planilla_recepcion.php'"><i class="icon-remove icon-white"></i> Cancelar</button>
</div>
</div>
<!-- ----------------------------->
<?php
require('piepagina.php');
?>