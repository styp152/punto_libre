-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-06-2014 a las 23:21:52
-- Versión del servidor: 5.5.37-MariaDB
-- Versión de PHP: 5.5.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gs_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canaima`
--

CREATE TABLE IF NOT EXISTS `canaima` (
  `serial_canaimita` int(11) NOT NULL,
  `modelo_canaimita` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `id_equipo` int(11) NOT NULL,
  `id_escuela` int(11) NOT NULL,
  PRIMARY KEY (`id_equipo`),
  UNIQUE KEY `serial_canaimita_UNIQUE` (`serial_canaimita`),
  KEY `fk_canaima_plantel1_idx` (`id_escuela`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `canaima`
--

INSERT INTO `canaima` (`serial_canaimita`, `modelo_canaimita`, `id_equipo`, `id_escuela`) VALUES
(234, 'N/S', 5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dueno`
--

CREATE TABLE IF NOT EXISTS `dueno` (
  `nacionalidad` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`cedula`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dueno`
--

INSERT INTO `dueno` (`nacionalidad`, `cedula`, `nombre`, `apellido`, `telefono`, `correo`) VALUES
('V', 6632363, 'Cloris', 'Lobo', '0412-241.41.24', 'cloris@correo.com'),
('V', 18124475, 'Lourdes', 'LObo', '0274-123.45.67', 'lou_lobo@correo.com'),
('V', 21394646, 'Miguel', 'Sanabria', '0414-321.54.65', 'correodemiguel@correo.com'),
('V', 23583416, 'Jesus', 'Vielma', '0414-757.25.84', 'jesusvielma309@gmail.com'),
('V', 23715376, 'Pedro', 'Peralta', '0426-098.76.54', 'pedro@correo.com'),
('V', 25560024, 'María', 'Vielma', '0412-072.30.06', 'maria@correo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE IF NOT EXISTS `equipo` (
  `id_equipo` int(11) NOT NULL AUTO_INCREMENT,
  `disco_duro` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo_equipo` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mem_ram` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cedula` int(11) NOT NULL,
  `cod_soporte` int(11) NOT NULL,
  PRIMARY KEY (`id_equipo`),
  UNIQUE KEY `id_equipo_UNIQUE` (`id_equipo`),
  KEY `fk_equipo_dueno_idx` (`cedula`),
  KEY `fk_equipo_soporte1_idx` (`cod_soporte`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`id_equipo`, `disco_duro`, `tipo_equipo`, `mem_ram`, `cedula`, `cod_soporte`) VALUES
(5, '', 'canaimita', '', 23583416, 5),
(7, 'gtcsd', 'portatil', 'fsdgfd', 23583416, 7),
(8, '60GB', 'escritorio', '512MB', 18124475, 8),
(9, '500GB', 'escritorio', '4GB', 6632363, 9),
(11, '320 GB', 'escritorio', '2 Gb', 25560024, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `estado` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `soporte_realizado` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_actualizacion` date NOT NULL,
  `cod_soporte` int(11) NOT NULL,
  PRIMARY KEY (`cod_soporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`estado`, `soporte_realizado`, `fecha_actualizacion`, `cod_soporte`) VALUES
('Terminado', 'Actualización de la lista de repositorio.\nActualización de las aplicaciones. \nOtra cosa', '2014-05-26', 5),
('En proceso', ' Actualizando SO ', '2014-05-26', 7),
('Terminado', '', '2014-05-21', 8),
('Sin iniciar', '', '2014-05-12', 9),
('En proceso', ' Respaldada la información.', '2014-05-30', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pc_escritorio`
--

CREATE TABLE IF NOT EXISTS `pc_escritorio` (
  `color_case` char(15) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_escritorio` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `id_equipo` int(11) NOT NULL,
  PRIMARY KEY (`id_equipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pc_escritorio`
--

INSERT INTO `pc_escritorio` (`color_case`, `descripcion_escritorio`, `id_equipo`) VALUES
('otro', 'lkmklmknhbbojkok98y76re4wasxdcfvgbhjn\r\nngyf56', 8),
('rojo', 'jcncncjn', 9),
('blanco', 'Esta como Nueva', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pc_portatil`
--

CREATE TABLE IF NOT EXISTS `pc_portatil` (
  `serial_portatil` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `modelo_portatil` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_portatil` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `id_equipo` int(11) NOT NULL,
  PRIMARY KEY (`id_equipo`),
  UNIQUE KEY `serial_portatil_UNIQUE` (`serial_portatil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pc_portatil`
--

INSERT INTO `pc_portatil` (`serial_portatil`, `modelo_portatil`, `descripcion_portatil`, `id_equipo`) VALUES
('12345r', 'sdfsdsf', 'klmlasvsfdv', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantel`
--

CREATE TABLE IF NOT EXISTS `plantel` (
  `id_escuela` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_plantel` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `ubicacion_plantel` text COLLATE utf8_spanish_ci NOT NULL,
  `grado_canaimita` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_escuela`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `plantel`
--

INSERT INTO `plantel` (`id_escuela`, `nombre_plantel`, `ubicacion_plantel`, `grado_canaimita`) VALUES
(3, '24 de Junio', 'Sucre', '1er grado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `soporte`
--

CREATE TABLE IF NOT EXISTS `soporte` (
  `cod_soporte` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_soporte` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_soporte` text COLLATE utf8_spanish_ci,
  `fecha_recepcion` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `responsable` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_soporte`),
  UNIQUE KEY `cod_soporte_UNIQUE` (`cod_soporte`),
  KEY `responsable` (`responsable`),
  KEY `responsable_2` (`responsable`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `soporte`
--

INSERT INTO `soporte` (`cod_soporte`, `tipo_soporte`, `descripcion_soporte`, `fecha_recepcion`, `fecha_entrega`, `responsable`) VALUES
(5, 'Actualizacion de software', 'Por favor actualizar los paquetes para tener el repositorio al día', '2014-05-12', '0000-00-00', 'pedro_perez'),
(7, 'Actualizacion de software', 'Actualizar el S.O a la versión mas nueva', '2014-05-12', '0000-00-00', 'juan_23'),
(8, 'Instalar S.O.', 'Instalar Canamia GNU/Linux', '2014-05-21', '0000-00-00', 'juan_23'),
(9, 'Otros', 'Revisar todo el sistemas operativo, limpieza de cosas tontas, ', '2014-05-12', '0000-00-00', 'j_vielma'),
(11, 'Instalar S.O.', 'Hacer un respaldo de toda la informacion, \r\nInstalar Canaima GNU/Linux como Sistema operativo', '2014-05-26', '0000-00-00', 'pedro_perez');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `canaima`
--
ALTER TABLE `canaima`
  ADD CONSTRAINT `fk_canaima_equipo1` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id_equipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_canaima_plantel1` FOREIGN KEY (`id_escuela`) REFERENCES `plantel` (`id_escuela`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `fk_equipo_dueno` FOREIGN KEY (`cedula`) REFERENCES `dueno` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_equipo_soporte1` FOREIGN KEY (`cod_soporte`) REFERENCES `soporte` (`cod_soporte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `fk_estado_soporte1` FOREIGN KEY (`cod_soporte`) REFERENCES `soporte` (`cod_soporte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pc_escritorio`
--
ALTER TABLE `pc_escritorio`
  ADD CONSTRAINT `fk_pc_escritorio_equipo1` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id_equipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pc_portatil`
--
ALTER TABLE `pc_portatil`
  ADD CONSTRAINT `fk_pc_portatil_equipo1` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id_equipo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
