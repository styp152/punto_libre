
</div>
</div>
<div class="push"></div>
</div>

<div class="footer">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12 ">
				<div class="span4">
					<p class="muted"><a href="http://puntolibre.gnu.org.ve/" target="_blank"><img src="../img/punto-small.png"/> Punto Libre 2013 <i class="icon-globe"></i></a></p>
				</div>
				<!--<div class="span3">
					<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">
					<img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a>
					<br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">licencia de Creative Commons Reconocimiento-CompartirIgual 3.0 Unported</a>.
				</div>-->
				<div class="span4">
					<p class="muted"><a href="http://gnu.org.ve/" target="_blank"><img src="../img/gnu-small.png"/> Proyecto GNU Venezuela 2013 <i class="icon-globe"></i> </a></p>
				</div>
				<div class="span4">
					<p class="muted"><a href="http://www.uptm.edu.ve" target="_blank">Universidad Politécnica Territorial del Estado Mérida <br/>"Kléber Ramírez"<i class="icon-globe"></i> </a></p>
				</div>
			</div>
		</div>
		</div>
		</div>		
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
<script type="text/javascript">
$('.form_date').datetimepicker({
        language:  'es',
        weekStart: 0,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		daysOfWeekDisabled: '0,6',
    });
											</script>
	</body>
</html>