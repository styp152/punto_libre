<?php
require('cabecera.php');
require('menu_lateral.php');
$codigo=$_POST['codigo'];
$realizado=$_POST['realizado'];
$responsable=$_POST['responsable'];
?>
				<div class="span9">
					<div class="hero-unit">
						<h3 class="text-center">Actualizar el estado de un soporte</h3>
						<br>
													
						<form method="post" action="actualizar_estado.php">
						<div class="row-fluid">
						<div class="span12">
							<div class="span4">
								<span>Código de Registro</span><br>
								<input class="input-block-level" type="text" name="cod_registro" title="Introduce el código de registro del equipo" required
								value="<?php echo $codigo?>" readonly/>
							</div>
							<div class="span4">
								<span>Fecha de Actualizacion</span><br>
							 <div class="input-append date form_date " data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="input-block-level" type="text" value="<?php echo date('d-m-Y');?>" required>
                    <!--<span class="add-on"><i class="icon-remove"></i></span>
																				Con esta Linea Activo un boton para elimnar la fecha de alli-->
																				<span class="add-on"><i class="icon-calendar"></i></span>
																								<input type="hidden" id="dtp_input2" name="fecha_actualizacion" value="<?php echo date('Y-m-d');?>" />
                </div>
							</div>
							<div class="span4">
								<span>Actualizado por:</span><br>
								<input class="input-block-level" type="text" name="respnsable" readonly value="<?php echo $responsable?>"/>
							</div>
						</div>
						</div>
						<br>
						<div class="row-fluid">
						<div class="span12">
							<div class="span6">
								<span><strong>Estado</strong><br/></span>
								<div data-toggle="buttons-radio">
								<label class="radio inline">
								<span class="btn btn-warning">En Proceso </span><input type="radio" name="estado" value="En proceso" hidden="hidden"	 required/></label>
								<label class="radio inline"><span class="btn btn-primary">Terminado </span><input type="radio" name="estado" value="Terminado" hidden/></label>
								</div>
							</div>
							<div class="span6">
								<span><strong>Especificación del Soporte realizado</strong></span>
								<textarea class="input-block-level" cols="36" rows="4" required name="realizado"><?php echo $realizado?> </textarea>
							</div>
						</div>
						</div>
						<br>
						<div class="row-fluid text-center">
						<div class="span12">
								<a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal"><i class="icon-ok icon-white"></i> Actualizar</a>
								<button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
						</div>
						</div>
					</div>
				</div>
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Advertencia de Seguridad</h3>
  </div>
  <div class="modal-body alert alert-block">
    <strong>¿Esta totalmente seguro que desea guardar los cambios efectuados sobre este registro de soporte.?</strong>
	 <p>Tome en cuenta que los cambios que realice seran almacenados inmediatamente, permimitiendo obtener el estado del soporte.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    <button class="btn btn-primary" type="submit"><i class="icon-ok icon-white"></i> Actulizar</button>
  </div>
</div>
					</form>
				</div>


<?php
require('pie.php');
?>