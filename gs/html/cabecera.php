<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<title>Gestión de Soporte</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<meta name="description" content="">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
		<link href="../css/bootstrap-responsive.css" rel="stylesheet">
		<link href="../css/bootstrap-datetimepicker.css" rel="stylesheet">

	</head>

	<body>
		<div class="wrapper">
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					 <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
					 </button>
					<a class="brand" href="index.php"><img src="../img/punto-small.png"/> Punto Libre</a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							<li><a href="../../Sistema_administrativo/html/menu_definitivo.php"><i class="icon-home"></i> Administración de P.L.</a></li>
							<li><a href="../../sme/html/index.php"><i class="icon-book"></i> Gestión Cursos</a></li>
							<li><a href="../../inventario/html/index.php"><i class="icon-tasks"></i> Organización Inventario</a></li>
							<li class="active dropdown">
								<a href="index.php" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-hdd"></i> Soporte a Equipos <b class="caret"></b></a>
								<ul class="dropdown-menu role="menu" aria-labelledby="dropdownMenu"">
									<li >
										<a tabindex="-1" href="cliente.php"><i class="icon-pencil"></i> Regsitrar </a>
										<!--<ul class="dropdown-menu role="menu" aria-labelledby="dropdownMenu"">
									<li ><a href="cliente.php" ><i class="icon-user"></i> Clientes </a></li>
									<li class="disabled"><a href="#" tabindex="-1"><i class="icon-hdd		"></i> Soportes </a></li>
									</ul>-->
									</li>
									<li><a href="consultaestado.php"><i class="icon-search "></i> Consulta Estado de Soporte </a></li>
									<li><a href="listarsoportes.php"><i class="icon-list "></i> Listado de Soportes </a></li>
								</ul>
							</li>
						</ul>
						<div class="pull-right">
							<ul class="nav pull-right">
								<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> <?php echo $_SESSION['usuario'];?> <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="#"><i class="icon-user"></i> Perfil</a></li>
										<li><a href="#"><i class="icon-headphones"></i> Ayuda General</a></li>
										<li class="divider"></li>
										<li><a href="../../Sistema_administrativo/html/paginaprincipal.php"><i class="icon-off"></i> Cerrar sesión</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br><br>